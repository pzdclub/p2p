#include <iostream>
#include "Network/P2PClient.h"
#include <boost/asio.hpp>
#include "Json/json.hpp"
#include <thread>
#include <time.h>
#include <fstream>

using namespace std;
using namespace boost::asio;

using json = nlohmann::json;


int main(int argc, char* argv[])
{
	p2p::P2PClient Self(11181);

	try{
		std::ifstream fin("nodes.txt");
		std::stringstream Stream;

		if(fin.is_open()){
			Stream << fin.rdbuf();
			json J = json::parse(Stream.str());

			std::vector<ip::udp::endpoint> Nodes = J["list"];

			for(ip::udp::endpoint Node : Nodes)
				Self.addPeer(Node, true);
		}
	}
	catch(...){}


	Self.setReceiveHandler([](auto Data, auto From, auto Error){
		if(Error != boost::system::errc::errc_t::success)
			return;
		std::cout << "Recived from " << From.address().to_string() << ":" << From.port() << " msg:\n"
		          << Data << "\n";
		std::flush(std::cout);
	});


	std::thread Sender([&Self]{
		std::string Msg;
		time_t Time;
		while(1){
			time(&Time);
			Msg = ctime(&Time);
			for(ip::udp::endpoint Peer : Self.Peerlist()){
				Self.Send(Peer, Msg);
			}
			sleep(500);
		}
	});
	Sender.detach();

	Self.Listen(true);
}
