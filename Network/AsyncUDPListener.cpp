#include "AsyncUDPListener.h"
#include <iostream>
#include <string>





AsyncUDPListener::AsyncUDPListener(int Port, io_service &Service):
    _Socket(Service, ip::udp::endpoint(ip::udp::v4(), Port)),
    _Listening(false),
    _Port(Port),
    _hSent([](auto a, auto b, auto c){}),
    _hReceive([](auto a, auto b, auto c){})
{}


void AsyncUDPListener::Listen(bool v)
{
	_Listening = v;

	if(v){
		char *Data = new char[MAX_BUFFER];
		ip::udp::endpoint *From = new ip::udp::endpoint;
		_Socket.async_receive_from(buffer(Data,MAX_BUFFER), *From, [this, Data, From](boost::system::error_code err, size_t bytes){
			RawData New;
			New.insert(0, Data, bytes);

			onReceive(New, *From, err);

			delete From;
			delete Data;
			Listen(_Listening);
		});
	}
}

void AsyncUDPListener::Send(ip::udp::endpoint To, RawData Data)
{
	_Socket.async_send_to(buffer(Data), To, [this, Data, To](boost::system::error_code err, size_t bytes){
		RawData New = std::move(Data);
		New.resize(bytes);
		onSent(New, To, err);
	});
}

void AsyncUDPListener::onReceive(RawData Data, ip::udp::endpoint From, boost::system::error_code e)
{
	_hReceive(Data, From, e);
}

void AsyncUDPListener::onSent(RawData Data, ip::udp::endpoint To, boost::system::error_code e)
{
	_hSent(Data, To, e);
}



void AsyncUDPListener::setReceiveHandler(AsyncUDPListener::Handler rh)
{
	_hReceive = rh;
}

void AsyncUDPListener::setSentHandler(AsyncUDPListener::Handler sh)
{
	_hSent = sh;
}
