#include "P2PClient.h"
#include <unistd.h>
#include <iostream>

using namespace p2p;




void nlohmann::adl_serializer<ip::udp::endpoint>::to_json(json &J, const ip::udp::endpoint Ep)
{
	J = json{{"address", Ep.address().to_string()}, {"port", Ep.port()}};
}
void nlohmann::adl_serializer<ip::udp::endpoint>::from_json(const json &J, ip::udp::endpoint &Ep)
{
	Ep = ip::udp::endpoint(ip::address::from_string(J["address"]), J["port"]);
}







P2PClient::P2PClient(int Port, io_service *Service):
    AsyncUDPListener(Port, *Service),
    _IOService(Service)
{
	_InactivePeersCollector = std::thread([this]{
		sleep(MAX_INACTIVE_SECONDS);
		while(isListening()){
			_Mutex.lock();
			for(auto &Peer : _Peerlist){
				Peer.second = false;
			}
			_Mutex.unlock();

			sleep(MAX_INACTIVE_SECONDS);

			_Mutex.lock();
			for(auto Peer : _Peerlist){
				if(!Peer.second && !isNode(Peer.first))
					_Peerlist.erase(Peer.first);
			}
			_Mutex.unlock();
		}
	});
	_Updater = std::thread([this]{
		sleep(PEERLIST_SYNC_INTERVAL);
		while(isListening()){
			UpdatePeerlist();
			sleep(PEERLIST_SYNC_INTERVAL);
		}
	});
}


void P2PClient::onSent(RawData Data, ip::udp::endpoint To, boost::system::error_code e)
{
	try{
		json Command = json::parse(Data);
		if(Command["service"] != true)
			throw "NotService";
	}
	catch(...){
		AsyncUDPListener::onSent(Data, To, e);
	}
}

void P2PClient::onReceive(RawData Data, ip::udp::endpoint From, boost::system::error_code e)
{
	LOCK l(_Mutex);

	if(e == boost::system::errc::errc_t::success){
		_Peerlist[From] = true;
	}

	try{
		json Command = json::parse(Data);
		if(Command["service"] != true)
			throw "NotService";

		ParceJsonCommand(Command, From);
	}
	catch(...){
		AsyncUDPListener::onReceive(Data, From, e);
	}

}



void P2PClient::ParceJsonCommand(json Data, ip::udp::endpoint From)
{
	if(Data["command"] == "update"){
		std::vector<ip::udp::endpoint> PeerList;

		for(std::pair<ip::udp::endpoint, int> Peer : _Peerlist){
			if(Peer.first != From)
				PeerList.push_back(Peer.first);
		}
		json Resp;

		Resp["service"] = true;
		Resp["command"] = "peerlist";
		Resp["list"] = PeerList;

		std::string Str = Resp.dump();
		Send(From, Str);
	}
	if(Data["command"] == "peerlist"){
		std::vector<ip::udp::endpoint> PeerList = Data["list"];
		for(ip::udp::endpoint Ep : PeerList){
			_Peerlist[Ep] = false;
		}
	}
}



void P2PClient::Listen(bool v)
{
	AsyncUDPListener::Listen(v);
	if(v){
		if(_InactivePeersCollector.joinable() && _Updater.joinable()){
			UpdatePeerlist();
			_InactivePeersCollector.detach();
			_Updater.detach();
			_IOService->run();
		}
	}
	else{
		_IOService->stop();
	}
}

void P2PClient::addPeer(ip::udp::endpoint Peer, bool isNode)
{
	_Peerlist[Peer] = true;
	if(isNode)
		_Nodelist[Peer] = true;
}


void P2PClient::UpdatePeerlist()
{
	LOCK l(_Mutex);

	json Command;
	Command["command"] = "update";
	Command["service"] = true;

	for(std::pair<ip::udp::endpoint,int> Peer : _Peerlist){
		this->Send(Peer.first, Command.dump());
	}
}


bool P2PClient::isNode(ip::udp::endpoint Peer)
{
	return _Nodelist[Peer];
}




