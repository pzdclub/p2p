#ifndef ASYNCUDPSERVER_H
#define ASYNCUDPSERVER_H

#include <boost/asio.hpp>
#include <memory>

using namespace boost::asio;



#define MAX_BUFFER 1048576





using RawData = std::string;


class AsyncUDPListener {
protected:
	using Handler = std::function<void(RawData&, ip::udp::endpoint&, boost::system::error_code&)>;
public:
	AsyncUDPListener(int Port, io_service &Service);


	virtual void Listen(bool v);


	void Send(ip::udp::endpoint To, RawData Data);


	virtual void onReceive(RawData Data, ip::udp::endpoint From, boost::system::error_code e);
	virtual void onSent(RawData Data, ip::udp::endpoint To, boost::system::error_code e);


	void setReceiveHandler(Handler rh);
	void setSentHandler(Handler sh);

	bool isListening(){return _Listening;}
	int Port(){return _Port;}

private:
	ip::udp::socket _Socket;

	bool _Listening;
	Handler _hReceive, _hSent;
	int _Port;
};







#endif // ASYNCUDPSERVER_H
