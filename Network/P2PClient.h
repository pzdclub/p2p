#ifndef P2PCLIENT_H
#define P2PCLIENT_H

#include "AsyncUDPListener.h"
#include "../Json/json.hpp"
#include <map>
#include <thread>
#include <mutex>


#define PEERLIST_SYNC_INTERVAL	10		//every *n* seconds download peerlist from each known peer
#define MAX_INACTIVE_SECONDS	20

namespace nlohmann {

template <>
struct adl_serializer<ip::udp::endpoint>{
	static void to_json(json &J, const ip::udp::endpoint Ep);
	static void from_json(const json &J, ip::udp::endpoint &Ep);
};

}


namespace p2p {
using json = nlohmann::json;


class P2PClient : public AsyncUDPListener {
	using LOCK = std::lock_guard<std::mutex>;
public:
	P2PClient(int Port, io_service *Service = new io_service());


	virtual void	onReceive(RawData Data, ip::udp::endpoint From, boost::system::error_code e);
	virtual void	onSent(RawData Data, ip::udp::endpoint To, boost::system::error_code e);
	virtual	void	Listen(bool v);




	void	addPeer(ip::udp::endpoint Peer, bool isNode = false);
	void	UpdatePeerlist();


	bool	isNode(ip::udp::endpoint Peer);


	void	ParceJsonCommand(json Data, ip::udp::endpoint From);


	std::vector<ip::udp::endpoint>	Peerlist(){return _MapToVector<ip::udp::endpoint,typeof(_Peerlist)>(_Peerlist);}
	std::vector<ip::udp::endpoint>	Nodelist(){return _MapToVector<ip::udp::endpoint,typeof(_Nodelist)>(_Nodelist);}
private:
	io_service		*_IOService;

	std::thread		_InactivePeersCollector;
	std::thread		_Updater;
	std::mutex		_Mutex;

	std::map<ip::udp::endpoint,bool>	_Peerlist;	//TODO: replace with unordered map
	std::map<ip::udp::endpoint,bool>	_Nodelist;


	template <class T, class MapType>
	std::vector<T>			_MapToVector(MapType &Map);
};


}





template <class T, class MapType>
std::vector<T> p2p::P2PClient::_MapToVector(MapType &Map)
{
	std::vector<T> Ret;
	for(auto Val : Map){
		Ret.push_back(Val.first);
	}

	return Ret;
}












#endif // P2PCLIENT_H
